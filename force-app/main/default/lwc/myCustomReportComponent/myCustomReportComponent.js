import { LightningElement, api, track, wire } from 'lwc';

import getReports from '@salesforce/apex/CustomerReport.getReports';
import getReportRecordData from '@salesforce/apex/CustomerReport.getReportRecordData';

export default class App extends LightningElement {
@track  files = []; 
@api recordId
  @wire (getReports,{accountId:'$recordId'})
   getReports({
     data,error
   }){
    if(data){
      this.files=data;
    }
    else if(error) {
      window.console.log('error ===> '+JSON.stringify(error));
  }
  
   }
  
   /*
   Customer_Report__c
   Account__c
   Name
   Data_Source__c
   Frequency__c
   FTP__c
   Note__c
   Parcel__c
   Schedule__c
   Template__c
   Title__c
   */
  Account__c='';
  Name='';
  Data_Source__c='';
  Frequency__c='';
  FTP__c='';
  Note__c='';
  Parcel__c='';
  Schedule__c='';
  Template__c='';
  Title__c='';
  
  picklistChange(event){
  let selectedVal =event.target.value;
 // console.log(selectedVaLUE);
  getReportRecordData({ picklistValue: selectedVal })
            .then((result) => {
                this.recordsData = result;
                this.Note__c = result[0].Note__c;
                this.Account__c= result[0].Account__c;
                this.Data_Source__c = result[0].Data_Source__c;
                this.Frequency__c = result[0].Frequency__c;
                this.FTP__c = result[0].FTP__c;
                this.Parcel__c = result[0].Parcel__c;
                this.Schedule__c = result[0].Schedule__c;
                this.Template__c = result[0].Template__c;
                this.Title__c = result[0].Title__c;
                this.Name = result[0].Name;
                this.error = undefined;
            })
            .catch((error) => {
                this.error = error;
                this.recordsData = undefined;
            });
}

handleSucess(event){
   
    this.accountId = event.detail.id;
 }

// handleSubmit(event) {
//      event.preventDefault();
//     const inputFields = event.detail.id;
//     console.log('hello form submit');
//     console.log(inputFields);
//     console.log('ggg');
//     console.log(this.recordsData[0]);

/**
 * Customer Report Name
 * Title
 * Template
 * Frequency
 * Note
 * Schedule
 * ICO
 * Parcel
 * FTP
 * Data Source
 * Customer Report
 * Account 
 * ======
 * Data_Source__c
 * Frequency__c
 * 	FTP__c
 * 	ICO__c
 * Note__c
 * 	Parcel__c
 * Schedule__c
 * Template__c
 * 	Title__c
 * Name
 */
   
//     this.template.querySelector('lightning-record-edit-form').submit(this.recordsData[0]);
// }

}