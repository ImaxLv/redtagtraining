public with sharing class CustomerReport {
    @AuraEnabled(cacheable=true)        
    public static  List<Customer_Report__c> getReports(String accountId) {
  //      cr='COD%';
    //    Customer_Report__c opp = [SELECT Id,Title__c,Template__c,Note__c,Schedule__c,Parcel__c,ICO__c,FTP__c,Frequency__c, Data_Source__c, Customer_Report__c FROM Customer_Report__c ];
        return [SELECT Id,Title__c,Template__c,Note__c,Schedule__c,Parcel__c,ICO__c,FTP__c,Frequency__c,Name, Data_Source__c, Customer_Report__c FROM Customer_Report__c WHERE  Account__c=:accountId];
    }

    @AuraEnabled(cacheable=true) 
    public static List<Customer_Report__c> getReportRecordData(String picklistValue) {
        return [SELECT Id,Title__c,Template__c,Note__c,Schedule__c,Parcel__c,ICO__c,FTP__c,Name,Frequency__c, Data_Source__c, Customer_Report__c FROM Customer_Report__c WHERE  Customer_Report__c=:picklistValue LIMIT 1];
    }

    
  
}