({
    doInit : function(component, event, helper) {
        helper.pageReference(component, event, helper);
    },
    reInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    doUpdate: function(component,event){
        var action = component.get("c.doUpdate");
        
        action.setCallback(this, function(response){ 
            var selectedRows = event.getParam('selectedRow');
            var navigateEvent = $A.get("e.force:editRecord");
            navigateEvent.setParams({ "recordId": selectedRow[0].Id  });
            navigateEvent.fire();
        });
         $A.enqueueAction(action);
    }
})